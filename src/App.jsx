import React, { Component } from 'react';
import { Button } from './components/Button';
import { Modal } from './components/Modal';

class App extends Component {
    state = {
        firstModalOpen: false,
        secondModalOpen: false,
    };

    toggleFirstModal = () => {
        this.setState(() => ({
            firstModalOpen: !this.state.firstModalOpen,
        }));
    };

    toggleSecondModal = () => {
        this.setState(() => ({
            secondModalOpen: !this.state.secondModalOpen,
        }));
    };

    handleOutsideFirstModal = event => {
        if (!event.target.closest('.modal')) {
            this.toggleFirstModal();
        }
    };

    handleOutsideSecondModal = event => {
        if (!event.target.closest('.modal')) {
            this.toggleSecondModal();
        }
    };

    render() {
        const firstModalActions = (
            <>
                <Button backgroundColor="#adff87" text="Confirm" />
                <Button backgroundColor="#69ff24" text="Cancel" onClick={this.toggleFirstModal} />
            </>
        );

        const secondModalActions = (
            <>
                <Button backgroundColor="#ff9891" text="Confirm" />
                <Button backgroundColor="#f74639" text="Cancel" onClick={this.toggleSecondModal} />
            </>
        );

        return (
            <div className="App">
                <Button backgroundColor="#fce770" text="Open first modal" onClick={this.toggleFirstModal} />
                <Button backgroundColor="#6a98cc" text="Open second modal" onClick={this.toggleSecondModal} />

                {this.state.firstModalOpen && (
                    <Modal
                        header="First Modal"
                        closeButton={true}
                        text="This is the content of the first modal."
                        actions={firstModalActions}
                        toggleModal={this.toggleFirstModal}
                        handleOutside={this.handleOutsideFirstModal}
                    />
                )}

                {this.state.secondModalOpen && (
                    <Modal
                        header="Second Modal"
                        closeButton={true}
                        text="This is the content of the second modal."
                        actions={secondModalActions}
                        toggleModal={this.toggleSecondModal}
                        handleOutside={this.handleOutsideSecondModal}
                    />
                )}
            </div>
        );
    }
}

export default App;
