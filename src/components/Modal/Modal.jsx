import React from 'react';
import './Modal.scss';

class Modal extends React.Component {
    render() {
        const { header, closeButton, text, actions, toggleModal, handleOutside } = this.props;

        return (
            <div className="modal-overlay" onClick={event => handleOutside(event)}>
                <div className="modal">
                    {closeButton && (
                        <span className="close-button" onClick={toggleModal}>
                            <svg
                                className="close-icon"
                                xmlns="http://www.w3.org/2000/svg"
                                height="48"
                                viewBox="0 0 48 48"
                                width="48"
                            >
                                <path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z" />
                                <path d="M0 0h48v48H0z" fill="none" />
                            </svg>
                        </span>
                    )}
                    <h2>{header}</h2>
                    <p>{text}</p>
                    <div className="modal-actions">{actions}</div>
                </div>
            </div>
        );
    }
}

export default Modal;

// onClick={closeButton ? toggleModal : null}
